#!/usr/bin/env node
const archiver = require('archiver');
const fs = require('fs');
const path = require('path');

async function run() {
  const out = process.env.FG_OUT;

  if (!out) {
    throw new Error('Missing FG_OUT environment variable');
  }

  const src = process.argv[2];
  const extname = process.argv[3];

  if (!src) {
    throw new Error('Missing source directory argument');
  }

  if (!extname) {
    throw new Error('Missing extension name argument')
  }

  console.log(`Bundling and deploying ${src} to ${out}/${extname}.ext`);

  return new Promise((resolve, reject) => {
    const archive = archiver('zip', { zlib: { level: 9 } });
    const output = fs.createWriteStream(path.resolve(out, `${extname}.ext`));

    output.on('close', resolve);
    output.on('error', reject);

    archive.on('error', reject);
    archive.on('warning', (err) => {
      if (err.code === 'ENOENT') {
        console.warn('ENOENT', err);
      } else {
        reject(err);
      }
    });

    archive.pipe(output);
    archive.directory(src, false);
    archive.finalize();
  });
}

run()
  .then(() => console.log('done'))
  .catch(console.error);
