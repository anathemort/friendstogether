#!/usr/bin/env node
const childProcess = require('child_process');
const fs = require('fs');
const path = require('path');
const { convert, create } = require('xmlbuilder2');
const readline = require('readline');

async function exec(command, args, options = {}) {
  return new Promise((resolve, reject) => {
    const proc = childProcess.spawn(command, args, options);

    proc.stdout.on('data', (data) => console.log(`[${command}]`, data.toString()));
    proc.stderr.on('data', (data) => {
      console.error(`[${command}]`, data.toString());
      reject(data.toString());
    });

    proc.on('exit', resolve);
  });
}

async function run() {
  const extXmlPath = process.argv[2];
  const newVersion = process.argv[3];

  if (!newVersion) {
    throw new Error('Please enter a new version number')
  }

  console.log('Updating version to', newVersion);

  const extXmlFile = path.resolve(extXmlPath);
  const extXmlContents = await fs.promises.readFile(extXmlFile, 'utf8');
  const extXml = convert(extXmlContents, { format: 'object' });

  extXml.root.properties.version = newVersion;
  extXml.root.announcement['@text'] = extXml.root.announcement['@text'].replace(/v(\d+\.\d+\.\d+)/, `v${newVersion}`);

  await fs.promises.writeFile(
    extXmlFile,
    create({ encoding: 'iso-8859-1' }, extXml).end({ indent: '    ', prettyPrint: true }),
    'utf8'
  );

  const changelogFile = path.resolve('CHANGELOG.md');
  let changelog = await fs.promises.readFile(changelogFile, 'utf8');
  changelog = changelog.replace(/Changelog/, `$&\n\n### ${newVersion}\n* `);

  await fs.promises.writeFile(changelogFile, changelog, 'utf8');

  console.log('NOTE: add release notes to CHANGELOG.md!');
  console.log('Press ENTER when done to create a commit.');

  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });

  await new Promise((resolve, reject) => {
    rl.question('', (answer) => {
      if (!answer) {
        resolve();
      } else {
        reject(`Aborting! (${answer})`);
      }
    });
  });

  console.log('Creating commit...');
  const commitMsg = await new Promise((resolve) => {
    rl.question('Enter a commit message: ', (answer) => {
      resolve(answer || 'updated version');
    });
  });

  rl.close();

  await exec('yarn', ['version', '--no-git-tag-version', '--new-version', newVersion]);
  await exec('git', ['commit', '-am', commitMsg]);
  await exec('git', ['tag', `v${newVersion}`]);
}

run().catch(console.error);
